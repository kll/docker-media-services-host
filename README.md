# Docker Media Services Host

## Disclaimer

This project is not in any way designed to be a simple one-click-to-deploy project that let's you chose which software you want to run and can be tailored to your needs. It's my personal media services host config that I use at home in order to retrieve media, serve them over the network and host a Nextcloud instance.

Because it's design is so heavily influenced by my personal tastes, you may prefer to just learn from it and take bits and pieces here and there rather than clone and run the whole thing as is.

Either way, I still try to be as concise as possible so that you can pretty much start from scratch with just this project and if someone happens to have a similar setup as me they can get up and running pretty easily just by reading this README.

In fact I did exactly that! This is heavily based on and influenced by [PlqnK](https://github.com/PlqnK/docker-media-services-host)

It leverages [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) in order to bring up/host the services. The services that are hosted on the server are:

- [samba](https://www.samba.org/): The standard Windows interoperability suite of programs for Linux and Unix.
- [Træfik](https://traefik.io/): A reverse-proxy that is very easy to configure and can automatically obtain Let's Encrypt certificates.
- [Portainer](https://www.portainer.io/): A simple management UI for Docker.

## Prerequisites

- A properly configured DNS server in your LAN as well as proper DNS entries with a domain suffix for your servers (populated by hand or automatically with the hostname of your devices).
- A paid domain name for which you have full control over.

## Installation

- Clone the repository.
- Copy all the `*.example*` files to versions without `.example`.
- Update every reference to `example.com` in the files with your personal domain name.
- Get a Plex claim token [here](https://www.plex.tv/claim/) and set the `PLEX_CLAIM` environment variable in the `docker-compose.override.yml` file with it.
- Adapt the rest of the variables in .env and other conf files according to your needs.
- Ensure all the required DNS entries exist where you host DNS.
- Ensure all the required DNS overrides exit in your local gateway.

On Arch Linux install htpasswd with:
```bash
sudo pacman -S apache
```

Then choose a password for the Træfik web interface and hash it as followed:

```bash
htpasswd -nb admin yourchosenpassword
```

Replace `yourpasswordhash` in `traefik.toml` under `entryPoints.traefik.auth.basic` with the hash that you just obtained.

Set a real email address under the [acme] section in `traefik.toml` so you can receive notifications about your certificates from LetsEncrypt.

You will need to create the docker proxy network and then you can then launch your containers:

```bash
docker network create proxy
docker-compose up -d
```

Run the post install script which will modify some services config files that were created during the first run:

```bash
sudo ./post-first-launch.sh
```